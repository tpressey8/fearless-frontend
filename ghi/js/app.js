window.addEventListener('DOMContentLoaded', async () => {

    function createCard(title, description, pictureUrl, dateStart, dateEnd) {
        return `
          <div class="card my-4 shadow-lg">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${title}</h5>
              <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${dateStart}-${dateEnd}
            </div>
          </div>
        `;
    }

    function alert(){
        const errorMessage = document.querySelector('.col');
        errorMessage.innerHTML = '<div class="alert alert-warning" role = "alert"> Uh oh! Something went wrong </div>';
    }

    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col');
    let colIndx = 0;

    try {
        const response = await fetch(url);

        if (!response.ok) {
            alert()
        } else {
            const data = await response.json();

            for (let conference of data.conferences){
                const detailURL = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailURL);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const description = details.conference.description;
                    const title = details.conference.name;
                    const pictureUrl = details.conference.location.picture_url;
                    const dateStart = new Date (details.conference.starts);
                    const newDateStart = dateStart.toLocaleDateString();
                    const dateEnd = new Date (details.conference.ends);
                    const newDateEnd = dateEnd.toLocaleDateString();
                    const html = createCard(title, description, pictureUrl, newDateStart, newDateEnd);
                    const column = columns[colIndx % 3];
                    column.innerHTML += html;
                    colIndx = (colIndx + 1) % 3;
                }
            }
        }
    } catch (e) {
        alert()
    }

});
