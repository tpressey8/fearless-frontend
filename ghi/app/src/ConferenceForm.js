import React, { useEffect, useState } from 'react';

function ConferenceForm () {

    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [startDate, setStartDate] = useState('')
    const [endDate, setEndDate] = useState('')
    const [description, setDescription] = useState('')
    const [maxPresentations, setMaxPresentations] = useState('')
    const [maxAttendees, setMaxAttendees] = useState('')
    const [location, setLocation] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
    }

    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);

          setName('');
          setStartDate('');
          setEndDate('');
          setDescription('');
          setMaxPresentations('');
          setMaxAttendees('');
          setLocation('');

        }
    }

    const fetchData = async () => {
        const locationUrl = 'http://localhost:8000/api/locations/';
        const response = await fetch(locationUrl);

        if(response.ok){
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">


              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>



              <div className="form-floating mb-3">
                <input value={startDate} onChange={handleStartDateChange} placeholder="Start Date" required type="date" name="starts" id="starts" className="form-control"  min="2023-02-23" max="2028-02-23" />
                <label htmlFor="starts">Starts</label>
              </div>


              <div className="form-floating mb-3">
                <input value={endDate} onChange={handleEndDateChange} placeholder="End Date" required type="date" name="ends" id="ends" className="form-control"  min="2023-02-23" max="2028-02-23" />
                <label htmlFor="ends">Ends</label>
              </div>


              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea value={description} onChange={handleDescriptionChange} placeholder="Description" required type="text" name="description" id="description" className="form-control"></textarea>
              </div>


              <div className="form-floating mb-3">
                <input value={maxPresentations} onChange={handleMaxPresentationsChange} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>

              <div className="form-floating mb-3">
                <input value={maxAttendees} onChange={handleMaxAttendeesChange} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>

              <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                            return (
                                <option value={location.id} key={location.id}>
                                    {location.name}
                                </option>
                            );
                        })};
                </select>
              </div>



              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ConferenceForm;
